#include <stdio.h>
#include <stdlib.h>
#include "ev3.h"
#include "ev3_port.h"
#include "ev3_tacho.h"
#include "ev3_sensor.h"
#include "sensors.h"


const char const *color[] = { "?", "BLACK", "BLUE", "GREEN", "YELLOW", "RED", "WHITE", "BROWN" };
#define COLOR_COUNT  (( int )( sizeof( color ) / sizeof( color[ 0 ])))

int get_color(void) {
int val;
uint8_t sn_color;
    if ( ev3_search_sensor( LEGO_EV3_COLOR, &sn_color, 0 )) {
       printf( "COLOR sensor is found, reading COLOR...\n" );
       if ( !get_sensor_value( 0, sn_color, &val ) || ( val < 0 ) || ( val >= COLOR_COUNT )) {
          val = 0;
          }
          //printf( "\r(%s) \n", color[ val ]);
          fflush( stdout );
    }
    return val;
}
   
float get_gyro_val(void) {
float value;
uint8_t sn_gyro;
    if (ev3_search_sensor(LEGO_EV3_GYRO, &sn_gyro,0)){
      printf("GYRO found, reading gyro...\n");
      if ( !get_sensor_value0(sn_gyro, &value )) {
        value = 0;
      }
      //printf( "\r(%f) \n", value);
      fflush( stdout );
    }
    return value;
}

float get_sonar_val(void) {
float value;
uint8_t sn_sonar;
    if (ev3_search_sensor(LEGO_EV3_US, &sn_sonar,0)){
      printf("SONAR found, reading sonar...\n");
      if ( !get_sensor_value0(sn_sonar, &value )) {
        value = 0;
      }
      printf( "\r(%f) \n", value);
      fflush( stdout );
    }
    return value;
}

