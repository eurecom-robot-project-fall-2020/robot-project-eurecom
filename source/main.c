#include <stdio.h>
#include <stdlib.h>
#include "ev3.h"
#include "ev3_port.h"
#include "ev3_tacho.h"
#include "ev3_sensor.h"
//#include "motor_movement.h"
#include "lifting_mechanism.h"
#include "sensors.h"
#include <signal.h>
// WIN32 /////////////////////////////////////////
#ifdef __WIN32__

#include <windows.h>

// UNIX //////////////////////////////////////////
#else

#include <unistd.h>
#define Sleep( msec ) usleep(( msec ) * 1000 )

//////////////////////////////////////////////////
#endif

int stop_signal_called = 0;
void sigint_handler(int code){
    (void)code;
    stop_signal_called = 1;
}

const char const *color1[] = { "?", "BLACK", "BLUE", "GREEN", "YELLOW", "RED", "WHITE", "BROWN" };

static bool _check_pressed( uint8_t sn )
{
  int val;

  if ( sn == SENSOR__NONE_ ) {
    return ( ev3_read_keys(( uint8_t *) &val ) && ( val & EV3_KEY_UP ));
  }
  return ( get_sensor_value( 0, sn, &val ) && ( val != 0 ));
}


int main( void )
{
  int i;
  uint8_t sn_arm;
  uint8_t sn_clamp;
  FLAGS_T state;
  char s[ 256 ];
  int color_value;
  float gyro_value;
  float sonar_value;
  int val;
  uint32_t n, ii;
  int arm_speed = 300;
  int arm_time = 1500;
  int clamp_speed = 300;
  int clamp_time = 1500;
#ifndef __ARM_ARCH_4T__
  /* Disable auto-detection of the brick (you have to set the correct address below) */
  char ev3_brick_addr[] = "192.168.0.204";

#endif
  if ( ev3_init() == -1 ) return ( 1 );
  //Run all sensors
  if(ev3_sensor_init() == -1) return(1);
#ifndef __ARM_ARCH_4T__
  printf( "The EV3 brick auto-detection is DISABLED,\nwaiting %s online with plugged tacho...\n", ev3_brick_addr );

#else
  printf( "Waiting tacho is plugged...\n" );

#endif
  while ( ev3_tacho_init() < 1 ) Sleep( 1000 );

  printf( "*** ( EV3 ) Hello! ***\n" );

  printf( "Found tacho motors:\n" );
  for ( i = 0; i < DESC_LIMIT; i++ ) {
    if ( ev3_tacho[ i ].type_inx != TACHO_TYPE__NONE_ ) {
      printf( "  type = %s\n", ev3_tacho_type( ev3_tacho[ i ].type_inx ));
      printf( "  port = %s\n", ev3_tacho_port_name( i, s ));
      printf("  port = %d %d\n", ev3_tacho_desc_port(i), ev3_tacho_desc_extport(i));
    }
  }

  printf( "Found sensors:\n" );
  for ( i = 0; i < DESC_LIMIT; i++ ) {
    if ( ev3_sensor[ i ].type_inx != SENSOR_TYPE__NONE_ ) {
      printf( "  type = %s\n", ev3_sensor_type( ev3_sensor[ i ].type_inx ));
      printf( "  port = %s\n", ev3_sensor_port_name( i, s ));
      if ( get_sensor_mode( i, s, sizeof( s ))) {
        printf( "  mode = %s\n", s );
      }
      if ( get_sensor_num_values( i, &n )) {
        for ( ii = 0; ii < n; ii++ ) {
          if ( get_sensor_value( ii, i, &val )) {
            printf( "  value%d = %d\n", ii, val );
          }
        }
      }
    }
  }

  // Ctrl+C will exit loop
  signal(SIGINT, &sigint_handler);
  fprintf(stderr, "Press Ctrl+C to stop streaming...\n");


  //Run motors in order from port A to D
      int port_arm=65;   // need to be replaced with the port of the arm motor
      int port_clamp=66;   // need to be replaced with the port of the arm motor
      
      // lifting mechanism
      // grab the ball
      lifting_mechanism_grab(port_arm, port_clamp, sn_arm,sn_clamp,arm_speed,clamp_speed,arm_time, clamp_time);
      // release the ball
      //lifting_mechanism_release(port_arm, port_clamp, sn_arm,sn_clamp,arm_speed,clamp_speed,arm_time, clamp_time);
 
 
      while(1) {
           if (stop_signal_called) break; 
           color_value = get_color();
           printf("Color : %s\n",color1[color_value]);
           gyro_value = get_gyro_val();
           printf("gyro value %f",gyro_value);
           sonar_value = get_sonar_val();
           printf("sonar value %f",sonar_value);
      }


  ev3_uninit();
  printf( "*** ( EV3 ) Bye! ***\n" );

  return ( 0 );
}
