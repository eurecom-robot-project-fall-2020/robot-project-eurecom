#include <string.h>
typedef struct
{
    char name[80];
    float x;//Do we use centimeters or in meters?
    float y;//For now I use meters
    float heading;//In 0-360 degrees
} Coordinates;



int main(void)
{
    //Skeleton for the main process
    // I consider having a NAV_MODULE for navigation and motor handling
    // ULTRASONIC detector
    // color sensor module has COLORSENSOR_detect_ball
    // ARM and CLAMP
    // As C has no classes (rip), I'll use the module name as a prefix for all calls

    Coordinates first_container;//Were should we move to be in front of the first container?
    strcpy(first_container.name, "First container where balls are located");
    first_container.x = 0.65;
    first_container.y = 1.75;
    first_container.heading = 0;

    Coordinates box_drop_into;
    strcpy(box_drop_into.name, "Top left box container");
    box_drop_into.x = 0.45;
    box_drop_into.y = 1.52;
    box_drop_into.heading = 180;

    Coordinates pyramid_container;
    strcpy(box_drop_into.name, "Pyramid");
    box_drop_into.x = 0.30;
    box_drop_into.y = 0.86;
    box_drop_into.heading = 180;


    NAV_MODULE_navigate_to(first_container);
    float box_distance = ULTRASONIC_MODULE_measure();
    NAV_MODULE_forward(box_distance - 10);//goes at 10cm distance from the box
    pick_up();//WARNING not handled situation if we can't get the ball

    NAV_MODULE_navigate_to(box_drop_into);
    float box_distance = ULTRASONIC_MODULE_measure();
    NAV_MODULE_forward(box_distance - 10);//goes at 10cm distance from the box
    release_ball();
}

int pick_up()
{
    // returns 0 if successfull, -1 if not
    for (int i=0; i<4; i++)
    {
        CLAMP_release();
        ARM_move_down();
        CLAMP_pick();
        ARM_move_up();
        if (COLORSENSOR_detect_ball())
        {
            return 0;
        }
    }
    return -1;
}
void release_ball()
{
    CLAMP_release();
}