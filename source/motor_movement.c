#include <stdio.h>
#include <stdlib.h>
#include "ev3.h"
#include "ev3_port.h"
#include "ev3_tacho.h"
#include "ev3_sensor.h"
#include "motor_movement.h"

void motor_run(uint8_t sn, int speed,int time){

      set_tacho_stop_action_inx( sn, TACHO_HOLD ); // set the stoping action eg: coast-turns off power to motor, brake - make it to stop quickly than coast,hold-makes it to maintain the position.
      set_tacho_speed_sp( sn, speed); // sets the target speed in tacho counts per second. use negative speed to reverse the direction.
      set_tacho_time_sp( sn, time );  // sets the time to run the motor
      set_tacho_ramp_up_sp( sn, 0 ); // Sets the time in milliseconds that it take the motor to up ramp from 0% to 100%. Valid values are 0 to 10000 (10 seconds). Default is 0
      set_tacho_ramp_down_sp( sn, 0 ); // Sets the time in milliseconds that it take the motor to ramp down from 100% to 0%. Valid values are 0 to 10000 (10 seconds). Default is 0.
      set_tacho_command_inx( sn, TACHO_RUN_TIMED ); // set the command to the motor based on the values in the above commands. run_timed - run the motor for the amount of time mentioned in the time_sp and then stops, run-forever,run-direct- will take effect immediatley,stop,run-to-abs-pos - Runs the motor to an absolute position specified by``position_sp`` and then stops the motor using the command specified in stop_action,run-to-rel-pos - run-to-rel-pos: Runs the motor to a position relative to the current position value. The new position will be current position + position_sp. When the new position is reached, the motor will stop using the command specified by stop_action,reset - sets to default values

}
