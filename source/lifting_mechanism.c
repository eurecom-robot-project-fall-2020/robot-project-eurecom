#include <stdio.h>
#include <stdlib.h>
#include "ev3.h"
#include "ev3_port.h"
#include "ev3_tacho.h"
#include "ev3_sensor.h"
#include "motor_movement.h"
#include "lifting_mechanism.h"

// WIN32 /////////////////////////////////////////
#ifdef __WIN32__

#include <windows.h>

// UNIX //////////////////////////////////////////
#else

#include <unistd.h>
#define Sleep( msec ) usleep(( msec ) * 1000 )

//////////////////////////////////////////////////
#endif

void lifting_mechanism_grab(int port_arm, int port_clamp,uint8_t sn_arm,uint8_t sn_clamp,int arm_speed,int clamp_speed,int arm_time,int clamp_time){

//clamp open
if ( ev3_search_tacho_plugged_in(port_clamp,0, &sn_clamp, 0 )) {

   motor_run(sn_clamp,clamp_speed,clamp_time);
   Sleep(clamp_time);

} else {
      printf( "LEGO_EV3_M_MOTOR clamp is NOT found\n" );
}

/* Wait tacho stop */
      Sleep( 5000 );

// lift down
if ( ev3_search_tacho_plugged_in(port_arm,0, &sn_arm, 0 )) {
      printf( "LEGO_EV3_M_MOTOR arm is found, run for %d sec...\n",arm_time );
      motor_run(sn_arm,arm_speed,arm_time);
      Sleep(arm_time);

      /* Wait tacho stop */
      Sleep( 1000 );

} else {
      printf( "LEGO_EV3_M_MOTOR arm is NOT found\n" );
}

//clamp close 
if ( ev3_search_tacho_plugged_in(port_clamp,0, &sn_clamp, 0 )) {

   motor_run(sn_clamp,-clamp_speed,clamp_time);
      Sleep(clamp_time);

} else {
      printf( "LEGO_EV3_M_MOTOR clamp is NOT found\n" ); 
}

/* Wait tacho stop */
      Sleep( 1000 );

// lift up
if ( ev3_search_tacho_plugged_in(port_arm,0, &sn_arm, 0 )) {
      printf( "LEGO_EV3_M_MOTOR arm is found, run for %d sec...\n",arm_time );
      motor_run(sn_arm,-arm_speed,arm_time);

      /* Wait tacho stop */
      Sleep(arm_time);
      Sleep( 1000 );
      
} else {
      printf( "LEGO_EV3_M_MOTOR arm is NOT found\n" );
}

}


void lifting_mechanism_release(int port_arm, int port_clamp,uint8_t sn_arm,uint8_t sn_clamp,int arm_speed,int clamp_speed,int arm_time,int clamp_time){

//clamp open
if ( ev3_search_tacho_plugged_in(port_clamp,0, &sn_clamp, 0 )) {

   motor_run(sn_clamp,clamp_speed,clamp_time);
      Sleep(clamp_time);

} else {
      printf( "LEGO_EV3_M_MOTOR clamp is NOT found\n" );
}

/* Wait tacho stop */
      Sleep( 100 );

}
