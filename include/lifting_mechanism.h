void lifting_mechanism_grab(int port_arm, int port_clamp,uint8_t sn_arm,uint8_t sn_clamp,int arm_speed,int clamp_speed,int arm_time,int clamp_time);
void lifting_mechanism_release(int port_arm, int port_clamp,uint8_t sn_arm,uint8_t sn_clamp,int arm_speed,int clamp_speed,int arm_time,int clamp_time);
